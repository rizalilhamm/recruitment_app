from django.contrib.auth.decorators import login_required
from django.http.response import HttpResponse
from django.shortcuts import render, redirect
from django.contrib import messages

from application.forms import ApplicationForm, TandaiLamaranForm, TentukanHasilAkhirForm
from recruitment.models import Lamaran, Lowongan

@login_required(login_url='/login')
def semua_lamaran(request):
    if not request.user.is_hcd():
        messages.add_message(request, messages.INFO, 'Access denied!', extra_tags='danger')
        return redirect('lowongan')
    
    semua_lamaran = Lamaran.objects.all().order_by('-tanggal_lamar')

    return render(request, 'lamaran/semua_lamaran.html', {'semua_lamaran': semua_lamaran})

@login_required(login_url='/login')
def semua_lamaran_ku(request):
    if request.user.role != 'Pelamar':
        messages.add_message(request, messages.INFO, 'Halaman hanya bisa diakses oleh pelamar!')
        return redirect('home')
        
    semua_lamaran_ku = Lamaran.objects.filter(pelamar=request.user).all()
    return render(request, 'lamaran/lamaran_ku.html', {'semua_lamaran_ku': semua_lamaran_ku})


@login_required(login_url='/login')
def lamar_lowongan(request, pk):
    if request.user.role != 'Pelamar':
        messages.add_message(request, messages.INFO, 'Halaman hanya bisa diakses oleh pelamar!')
        return redirect('lowongan_detail', pk=pk)
        
    lowongan = Lowongan.objects.get(pk=pk)
    if request.method == 'POST':
        apply_form = ApplicationForm(request.POST, request.FILES)
        
        if apply_form.is_valid():
            cv_pelamar = apply_form.cleaned_data['cv_pelamar']
            portofolio_link = apply_form.cleaned_data['portofolio_link']
            
            lamaran_baru = Lamaran(pelamar=request.user, lowongan=lowongan, cv_pelamar=cv_pelamar, portofolio_link=portofolio_link)
            lamaran_baru.save()
            messages.add_message(request, messages.INFO, 'Lamaran berhasil terkirim!')
            return redirect('lamaranku')

    else:
        apply_form = ApplicationForm()
    return render(request, 'lamaran/lamar_lowongan.html', {'lowongan': lowongan, 'apply_form': apply_form})


@login_required(login_url='/login')
def detail_lamaran(request, pk):
    lamaran = Lamaran.objects.get(pk=pk)
    if request.method == 'POST':
        tandai_form = TandaiLamaranForm(request.POST)
        if tandai_form.is_valid():
            lamaran.status = tandai_form.cleaned_data['status']
            lamaran.save()

            if lamaran.status == 'Lulus':
                messages.add_message(request, messages.INFO, 'Peserta {} dinyatakan {}'.format(lamaran.pelamar.nama, lamaran.status), extra_tags='success')
            if lamaran.status == 'Tidak lulus':
                messages.add_message(request, messages.INFO, 'Peserta {} dinyatakan {}'.format(lamaran.pelamar.nama, lamaran.status), extra_tags='danger')
            return redirect('lowongan_detail', pk=lamaran.lowongan.pk)

    else:
        tandai_form = TandaiLamaranForm()
    return render(request, 'lamaran/lamaran_detail.html', {'lamaran': lamaran, 'tandai_form': tandai_form})


@login_required(login_url='/login')
def kirim_hasil_test(request, pk):
    select_lamaran = Lamaran.objects.get(pk=pk)
    if (select_lamaran.hasil_temu_bakat and select_lamaran.hasil_psikologi and select_lamaran.hasil_coderbyte) != '':
        messages.add_message(request, messages.INFO, 'Anda sudah mengirim hasil ujian')
        return render(request, 'lamaran/hasil_test.html', {'select_lamaran': select_lamaran})

    if request.method == 'POST':
        select_lamaran.hasil_temu_bakat = request.POST['hasil_temu_bakat']
        select_lamaran.hasil_psikologi = request.POST['hasil_psikologi']
        select_lamaran.hasil_coderbyte = request.POST['hasil_coderbyte']
        select_lamaran.hasil_akhir = 'Reviewing'
        select_lamaran.save()
        messages.add_message(request, messages.INFO, 'Hasil test anda sudah dikirim, dan siap untuk diperiksa oleh team HCD')
        return redirect('lamaranku')

    return render(request, 'lamaran/kirim_hasil_test.html', {'select_lamaran': select_lamaran})

@login_required(login_url='/login')
def hasil_akhir(request, pk):
    if not request.user.is_korektor():
        return redirect('home')
        
    select_lamaran = Lamaran.objects.get(pk=pk)
    if not request.user.is_korektor():
        return redirect('home')

    if not request.user.is_korektor():
        return redirect('kirim_hasil_test', pk=select_lamaran.pk)

    if request.method == 'POST':
        tentukan_hasil_akhir_form = TentukanHasilAkhirForm(request.POST)
        if tentukan_hasil_akhir_form.is_valid():
            select_lamaran.hasil_akhir = tentukan_hasil_akhir_form.cleaned_data['hasil_akhir']
            select_lamaran.save()
            messages.add_message(request, messages.INFO, '{} dinyatakan {}'.format(select_lamaran.pelamar.nama, select_lamaran.hasil_akhir))
            return redirect('lowongan_detail', pk=select_lamaran.pk)
    else:
        tentukan_hasil_akhir_form = TentukanHasilAkhirForm(request.POST)

    return render(request, 'lamaran/hasil_test.html', {
        'tentukan_hasil_akhir_form': tentukan_hasil_akhir_form ,
        'select_lamaran': select_lamaran}
        )