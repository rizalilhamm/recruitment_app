from django import forms
from recruitment.models import Lamaran

class ApplicationForm(forms.ModelForm):
    class Meta:
        model = Lamaran
        fields = ('cv_pelamar', 'portofolio_link',)

class TandaiLamaranForm(forms.ModelForm):
    class Meta:
        model = Lamaran
        fields = ('status',)

class TentukanHasilAkhirForm(forms.ModelForm):
    class Meta:
        model = Lamaran
        fields = ('hasil_akhir',)