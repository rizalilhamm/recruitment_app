from django.urls import path
from application import views

urlpatterns = [
    path('semua_lamaran/', views.semua_lamaran, name='semua_lamaran'),
    path('lamaranku/', views.semua_lamaran_ku, name='lamaranku'),
    path('lowongan/<int:pk>/lamar', views.lamar_lowongan, name='lamar_lowongan'),
    # path('login', views.lowongan, name='lowongan')
    path('lamaran/<int:pk>', views.detail_lamaran, name='detail_lamaran'),
    path('lamaranku/<int:pk>/kirim_hasil_test', views.kirim_hasil_test, name='kirim_hasil_test'),
    path('lowongan/<int:pk>/hasil_test/', views.hasil_akhir, name='hasil_akhir'),
]