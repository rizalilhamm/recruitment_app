from django.urls import path
from accounts import views

urlpatterns = [
    path('user_profile', views.profile_page, name='user_profile'),
    path('register', views.register_page, name='register'),
    path('login', views.login_page, name='login'),
    path('logout', views.logout_page, name='logout'),
    # path('login', views.lowongan, name='lowongan')
]