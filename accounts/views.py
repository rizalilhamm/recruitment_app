from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect
from django.contrib import messages
from recruitment.models import CustomUser
from accounts.forms import RegisterForm, LoginForm


# Create your views here.
def register_page(request):
    if request.user.is_authenticated:
        messages.add_message(request, messages.INFO, 'Anda masih login!', extra_tags='danger')
        return redirect('home')

    if request.method == 'POST':
        register_form = RegisterForm(request.POST)
        if register_form.is_valid():
            register_form.save()

            messages.add_message(request, messages.INFO, 'Register berhasil!',extra_tags='success')
            return redirect('login')
    
        messages.add_message(request, messages.INFO, 'Register gagal', extra_tags='danger')
        return redirect('register')
    else:
        register_form = RegisterForm()
    return render(request, 'accounts/register.html', {'register_form': register_form})

def login_page(request):
    if request.user.is_authenticated:
        return redirect('home')
    
    if request.method == 'POST':
        email = request.POST.get('email')
        password = request.POST.get('password')
        user = authenticate(email=email, password=password)
        if user:
            login(request, user)
            messages.add_message(request, messages.INFO, 'Login berhasil', extra_tags='success')
            return redirect('home')
        
        messages.add_message(request, messages.ERROR, 'Login Gagal', extra_tags='danger')
        return redirect('login')

    return render(request, 'accounts/login.html')

@login_required(login_url='/login')
def logout_page(request):
    logout(request)
    return redirect('login')


@login_required(login_url='/login')
def profile_page(request):
    return render(request, 'accounts/user_profile.html', {})