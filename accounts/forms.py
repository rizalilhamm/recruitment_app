from django import forms
from django.contrib.auth.forms import UserCreationForm

from recruitment.models import CustomUser


class RegisterForm(UserCreationForm):
    
    class Meta:
        model = CustomUser
        fields = ['email', 'nama', 'bio', 'role', 'password1', 'password2']

class LoginForm(forms.ModelForm):
    class Meta:
        model = CustomUser
        fields = ['email', 'password']