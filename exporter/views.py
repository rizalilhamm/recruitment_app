import csv
from django.http import HttpResponse

from exporter.utils import render_to_pdf
from recruitment.models import Lamaran


# Create your views here.
def export_csv(request):
    response = HttpResponse(content_type='text/csv')
    
    writer = csv.writer(response)
    writer.writerow(['Pelamar ID', 'Lowongan ID', 'Tanggal Lamaran', 'CV Pelamar', 'Portofolio', 'Status'])
    for lamaran in Lamaran.objects.all().values_list('pelamar', 'lowongan', 'tanggal_lamar', 'cv_pelamar', 'portofolio_link', 'status'):
        writer.writerow(lamaran)

    response['Content-Disposition'] = 'attactment; filename="csv_lamaran.csv"'
    return response


def export_pdf(request):
    semua_lamaran = Lamaran.objects.all()
    data = {'semua_lamaran': semua_lamaran}
    pdf = render_to_pdf('lamaran/export_pdf.html', data)
    return HttpResponse(pdf, content_type='application/pdf')