from django.urls import path
from exporter import views

urlpatterns = [
    path('csv/', views.export_csv, name='export_csv'),
    path('pdf/', views.export_pdf, name='export_pdf'),
]