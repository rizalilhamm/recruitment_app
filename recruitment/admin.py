from django.contrib import admin
from recruitment.models import CustomUser,Lowongan, Lamaran

# Register your models here.
admin.site.register(CustomUser)
admin.site.register(Lowongan)
admin.site.register(Lamaran)