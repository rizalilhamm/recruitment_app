import datetime
from django.contrib.auth.models import AbstractBaseUser, Group, PermissionsMixin
from django.db import models
from django.db.models.enums import Choices
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _

from .managers import CustomUserManager

# Create your models here
class CustomUser(AbstractBaseUser, PermissionsMixin):
    ROLE_CHOISES = [
        ('HCD', 'HCD'),
        ('Korektor', 'Korektor'),
        ('Pelamar', 'Pelamar')
    ]

    email = models.EmailField(_('Email Address'), unique=True)
    nama = models.CharField(max_length=30, blank=True)
    no_telpon = models.IntegerField(null=True, blank=True)
    bio = models.CharField(max_length=80, blank=True)
    is_staff = models.BooleanField(default=False)
    is_active = models.BooleanField(default=True)
    date_joined = models.DateTimeField(default=timezone.now)
    role = models.CharField(max_length=30, choices=ROLE_CHOISES, default='Pelamar')

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []

    objects = CustomUserManager()

    def __str__(self):
        return self.email

    def is_hcd(self):
        return self.role == 'HCD'
    
    def is_korektor(self):
        return self.role  == 'Korektor'

    def is_pelamar(self):
        return self.role == 'Pelamar'


class Lowongan(models.Model):
    judul_lowongan = models.CharField(max_length=100)
    deskripsi_lowongan = models.TextField()
    tanggal_buka = models.DateField(auto_now_add=True)
    tanggal_tutup = models.DateField()
    pengiklan = models.ForeignKey(CustomUser, on_delete=models.CASCADE, related_name='pengiklan')
    pelamar = models.ManyToManyField(CustomUser, through='Lamaran', related_name='pelamar_iklan')

    def __str__(self):
        return self.judul_lowongan


def user_directory_path(instance, filename):
    return 'user_{}/{}'.format(instance.pelamar.id, filename)

class Lamaran(models.Model):
    STATUS_CHOISES = [
        ('Reviewing', 'Reviewing'),
        ('Lulus', 'Lulus'),
        ('Tidak lulus', 'Tidak lulus')
    ]

    pelamar = models.ForeignKey(CustomUser, on_delete=models.CASCADE)
    lowongan = models.ForeignKey(Lowongan, on_delete=models.CASCADE)
    # 
    cv_pelamar = models.FileField(upload_to=user_directory_path)
    portofolio_link = models.URLField(blank=True)
    tanggal_lamar = models.DateField(default=datetime.date.today)
    status = models.CharField(max_length=30, choices=STATUS_CHOISES, default='Reviewing')

    test_temu_bakat = models.URLField(blank=True)
    test_psikologi = models.URLField(blank=True)
    test_coderbyte = models.URLField(blank=True)

    hasil_temu_bakat = models.URLField(blank=True)
    hasil_psikologi = models.URLField(blank=True)
    hasil_coderbyte = models.URLField(blank=True)

    hasil_akhir = models.CharField(max_length=30, choices=[
        ('Lulus', 'Lulus'),
        ('Tidak Lulus', 'Tidak Lulus'),
        ('Reviewing', 'Reviewing')
    ], blank=True)

    def __str__(self):
        return f"{self.lowongan}: {self.pelamar}"
