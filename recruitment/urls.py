from django.urls import path
from recruitment import views

urlpatterns = [
    path('', views.home, name='home'),
    path('home/', views.home, name='home'),
    path('lowongan/', views.lowongan, name='lowongan'),
    path('lowongan/tambah', views.lowongan_tambah, name='lowongan_tambah'),
    path('lowongan/<int:pk>/', views.lowongan_detail, name='lowongan_detail'),
    path('lowongan/<int:pk>/update', views.lowongan_update, name='lowongan_update'),
    path('lowongan/<int:pk>/hapus', views.lowongan_hapus, name='lowongan_hapus'),
]