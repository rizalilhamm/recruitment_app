from django.contrib import messages
from django.shortcuts import redirect, render, HttpResponse
from django.contrib.auth.decorators import login_required
from recruitment.models import Lowongan, Lamaran
from application.forms import TandaiLamaranForm


# Create your views here.
def home(request):
    return render(request, 'home.html', {'title': 'Dashboard'})


def lowongan(request):
    semua_lowongan = Lowongan.objects.all()
    return render(request, 'lowongan/lowongan_semua.html', {'title': 'Lowongan', 'semua_lowongan': semua_lowongan})

@login_required(login_url='/login')
def lowongan_tambah(request):
    if not request.user.is_hcd():
        return redirect('lowongan')
    if request.method == 'POST':
        judul_lowongan = request.POST.get('judul_lowongan')
        deskripsi_lowongan = request.POST.get('deskripsi_lowongan')
        tanggal_tutup = request.POST.get('tanggal_tutup')
        lowongan_baru = Lowongan(judul_lowongan=judul_lowongan, deskripsi_lowongan=deskripsi_lowongan, tanggal_tutup=tanggal_tutup)
        lowongan_baru.pengiklan = request.user
        lowongan_baru.save()
        messages.add_message(request, messages.INFO, 'Lowongan ditambahkan', extra_tags='success')
        return redirect('lowongan')
                
    return render(request, 'lowongan/tambah_lowongan.html')

@login_required(login_url='/login')
def lowongan_detail(request, pk):
    lowongan = Lowongan.objects.get(pk=pk)
    semua_lamaran = Lamaran.objects.filter(lowongan=lowongan).all()
    
    return render(request, 'lowongan/lowongan_detail.html', {'lowongan': lowongan, 'semua_lamaran': semua_lamaran})

@login_required(login_url='/login')
def lowongan_update(request, pk):
    if not request.user.is_authenticated:
        messages.add_message(request, messages.ERROR, 'Login untuk melanjutkan', extra_tags='danger')
        return redirect('login')
    
    if not request.user.is_hcd():
        return redirect('lowongan_detail', pk=pk)
    
    lowongan = Lowongan.objects.get(pk=pk)
    if lowongan is None:
        messages.add_message(request, messages.INFO, 'Lowongan tidak ditemukan', extra_tags='danger')
        return redirect('lowongan')

    if request.user.role == 'HCD' and request.method == 'POST':
        lowongan.judul_lowongan = request.POST.get('judul_lowongan')
        lowongan.deskripsi_lowongan = request.POST.get('deskripsi_lowongan')
        lowongan.tanggal_tutup = request.POST.get('tanggal_tutup')
        
        lowongan.save()
        messages.add_message(request, messages.INFO, '{} Berhasil di update'.format(lowongan.judul_lowongan), extra_tags='success')
        return redirect('lowongan_detail', pk=lowongan.pk)
        
    return render(request, 'lowongan/lowongan_update.html', {'lowongan': lowongan})

@login_required(login_url='/login')
def lowongan_hapus(request, pk):
    if request.user.role != 'HCD':
        messages.add_message(request, messages.ERROR, 'Access denied!', extra_tags='danger')
        return redirect('lowongan_detail', pk=pk)
    lowongan = Lowongan.objects.get(pk=pk)
    if request.method == 'POST':
        lowongan.delete()
        messages.add_message(request, messages.INFO, f'Lowongan {lowongan} dihapus', extra_tags='success')
        return redirect('lowongan')

    return render(request, 'lowongan/lowongan_hapus.html', {'lowongan': lowongan})
