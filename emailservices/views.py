from django.shortcuts import redirect, render
from django.core.mail import send_mail
from django.contrib import messages

from recruitment_app.settings import EMAIL_HOST_USER
from recruitment.models import Lamaran, Lowongan

# Create your views here.
def lulus_berkas_email(request, pk):
    select_lamaran = Lamaran.objects.get(pk=pk)
    # print(select_lamaran.pelamar.email)
    if request.method == 'POST':
        subject = 'Notifikasi Lulus'
        select_lamaran.test_temu_bakat = request.POST['temu_bakat_test']
        select_lamaran.test_psikologi = request.POST['psikologi_test']
        select_lamaran.test_coderbyte = request.POST['codebyte_test']
        message = f"""
                Dear {select_lamaran.pelamar.nama},

                Selamat anda pada {select_lamaran.lowongan} dinyatakan lulus seleksi berkas.
                berikut test lanjutan yang harus dikerjakan untuk tahap selanjutnya:
                1. Temu bakat           {select_lamaran.test_temu_bakat}
                2. Test Psikologi       {select_lamaran.test_psikologi}
                3. Codebyte challages   {select_lamaran.test_coderbyte}

                Untuk pengumpulan simpan Screeshot masing-masing hasil test di Google docs (Pastikan akses untuk public).
                kemudian input link di form pengumpulan pada akun RekrutmenApp anda

                selamat mengerjakan!

                Terima kasih,
                Team Rekrutmen.
                """
        recepients = [select_lamaran.pelamar.email]
        send_mail(subject, message, EMAIL_HOST_USER, recepients)
        select_lamaran.save()
        messages.add_message(request, messages.INFO, f'Notifikasi Lulus sudah dikirim ke {select_lamaran.pelamar}')
        return redirect('lowongan_detail', pk=select_lamaran.lowongan.pk)
        
    return render(request, 'email/lulus_berkas.html', {'select_lamaran': select_lamaran})

def tidak_lulus_berkas_email(request, pk):
    select_lamaran = Lamaran.objects.get(pk=pk)
    subject = 'Notifikasi Tidak Lulus'
    message = f"""
            Dear {select_lamaran.pelamar.nama},
            
            Kami sangat terkesan dengan CV dan Portofolio anda, namun karena keahlian yang anda miliki, belum sesuai dengan kebutuhan kami,
            maka terpaksa aplikasi anda tidak bisa kami lanjutkan ke tahap selanjutnya.

            Jika anda merasa tertarik untuk lowongan lainnya silahkan cek di situs perusahaan kami. 
            
            Salam Sukses!
            
            Terima kasih,
            Team Rekrutmen.
            """
    if request.method == 'POST':
        recepients = [select_lamaran.pelamar.email]
        send_mail(subject, message, EMAIL_HOST_USER, recepients)
        messages.add_message(request, messages.INFO, f'Notifikasi tidak Lulus sudah dikirim ke {select_lamaran.pelamar}')
        return redirect('lowongan_detail', pk=select_lamaran.lowongan.pk)

    return render(request, 'email/tidak_lulus_berkas.html', {'select_lamaran': select_lamaran, 'message': message})