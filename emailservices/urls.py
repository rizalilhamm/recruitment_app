from django.urls import path
from emailservices import views

urlpatterns = [
    path('lowongan/<int:pk>/lulus_notifikasi', views.lulus_berkas_email, name='lulus_berkas_email'),
    path('lowongan/<int:pk>/tidak_lulus_notifikasi', views.tidak_lulus_berkas_email, name='tidak_lulus_berkas_email'),
]